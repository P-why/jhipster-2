default:
  image: node:16-bullseye-slim
  cache:  # Cache modules in between jobs
    key: $CI_COMMIT_REF_SLUG
    paths:
      - node_modules/
      - ~/.m2/repository/
  before_script:
    ## - chmod 744 mvnw
    # Installing java and maven
    - apt-get update && apt-get install -y maven openjdk-11-jdk
    - mvn --version
    - java -version
    - npm install

variables:
  TRAEFIK_IP: 34.163.255.160
  IMAGE_NAME: jhipster
  IMAGE_TAG_DEV: dev-0.1.${CI_MERGE_REQUEST_IID}
  IMAGE_FULL_PATH_DEV: ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${IMAGE_TAG_DEV}
  IMAGE_TAG_PROD: 0.1.${CI_COMMIT_SHORT_SHA}
  IMAGE_FULL_PATH_PROD: ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${IMAGE_TAG_PROD}
  KUBECTL_APPLY_COMMAND_DEFAULT: kubectl apply -f ./kubernetes/duo-jhipster-deployment.yaml
  KUBECTL_DELETE_COMMAND_DEFAULT: kubectl delete -f ./kubernetes/duo-jhipster-deployment.yaml
  

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stages:
  - build
  - kaniko
  - verify
  - test
  - visualize
  - deploy
  - cleanup

### Build ###
# Build the java code into a .jar file
build:jar-dev:
  extends: .build:jar-script-prod-template
  rules:
    - if: $CI_MERGE_REQUEST_IID
  variables:
    MVNW_COMMAND: ./mvnw clean package

build:jar-prod:
  extends: .build:jar-script-prod-template
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  variables:
    MVNW_COMMAND: ./mvnw -Pprod clean package

### Kaniko ###
# Build a docker image with the .jar file, and upload it in gitlab container registry
kaniko:dev:
  rules:
    - if: $CI_MERGE_REQUEST_IID
  extends: .kaniko:template
  variables:
    IMAGE_NAME: jhipster
    IMAGE_TAG: ${IMAGE_TAG_DEV}
    JHIPSTER_IMAGE_FULL_NAME: ${IMAGE_FULL_PATH_DEV}

kaniko:prod:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  extends: .kaniko:template
  variables:
    IMAGE_NAME: jhipster
    IMAGE_TAG: ${IMAGE_TAG_PROD}
    JHIPSTER_IMAGE_FULL_NAME: ${IMAGE_FULL_PATH_PROD}
    

### Verify ###
test:verify:is-traefik-up:
  cache: []
  stage: verify
  image: 
    name: bitnami/kubectl:1.29.0-debian-11-r1
    entrypoint: [""]
  before_script:
    - kubectl config use-context P-why/jhipster-2:kubernetagent
  script:
    - kubectl apply -f kubernetes/traefik-ingress.yaml

### Tests ###
# Check traefik + add dashboard. Can be disabled after first launch
test:backend-java:
  stage: test
  script:
    # - ./mvnw clean verify
    - ./mvnw clean test
    - echo "Displaying Jacoco report"
    - cat target/site/jacoco/index.html
  artifacts:
    when: always
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml
        # - target/failsafe-reports/TEST-*.xml
  coverage: '/Total.*?([0-9]{1,3})%/'

test:frontend-npm:
  stage: test
  script:
    - npm test
  artifacts:
    when: always
    reports:
      junit:
        - target/test-results/TEST*.xml
  coverage: '/All files[^|]*\|[^|]*\s+([\d\.]+)/'

### Visualize ###
.visualize:coverage-backend:
  cache: []
  stage: visualize
  image: registry.gitlab.com/haynes/jacoco2cobertura:1.0.9
  script:
    # convert report from jacoco to cobertura, using relative project path
    - python /opt/cover2cover.py target/site/jacoco/jacoco.xml $CI_PROJECT_DIR/src/main/java/ > target/site/cobertura.xml
  needs: ["test:jacoco"]
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: target/site/cobertura.xml

### Deploy ###
# Deployment dev - only if on a branch of Merge Request
deploy:dev:
  rules:
    - if: $CI_MERGE_REQUEST_IID
  extends: .deploy
  variables:
    JHIPSTER_NAMESPACE: dev-${CI_COMMIT_REF_SLUG}
    JHIPSTER_APP_NAME: dev-jhipster-${CI_COMMIT_REF_SLUG}
    JHIPSTER_POSTGRES_NAME: disabled
    JHIPSTER_FINAL_URL: ${CI_COMMIT_REF_SLUG}.dev.gr2.eqldog.space
    JHIPSTER_IMAGE_FULL_NAME: ${IMAGE_FULL_PATH_DEV}
    SPRING_DATASOURCE_URL: jdbc:postgresql://localhost:5432/jhipsterSampleApplication
    SPRING_LIQUIBASE_URL: jdbc:postgresql://localhost:5432/jhipsterSampleApplication
    KUBECTL_APPLY_COMMAND: ${KUBECTL_APPLY_COMMAND_DEFAULT} --selector=app!=${JHIPSTER_POSTGRES_NAME}
  environment:
    name: review/dev-${CI_COMMIT_REF_SLUG}
    url: http://${JHIPSTER_FINAL_URL}
    auto_stop_in: 2 days
    on_stop: cleanup:delete-dev

# Deployment ppd - only if branch main
deploy:ppd:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  extends: .deploy
  variables:
    JHIPSTER_NAMESPACE: ppd
    JHIPSTER_APP_NAME: jhipster-${JHIPSTER_NAMESPACE}
    JHIPSTER_POSTGRES_NAME: postgres-${JHIPSTER_NAMESPACE}
    JHIPSTER_FINAL_URL: ${JHIPSTER_NAMESPACE}.gr2.eqldog.space
    JHIPSTER_IMAGE_FULL_NAME: ${IMAGE_FULL_PATH_PROD}
    KUBECTL_APPLY_COMMAND: ${KUBECTL_APPLY_COMMAND_DEFAULT}
  environment:
    name: ppd
    url: http://${JHIPSTER_FINAL_URL}
    on_stop: cleanup:delete-ppd

# Deployment prod - only if branch main, and manual
deploy:prod:
  extends: .deploy
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
  variables:
    JHIPSTER_NAMESPACE: prod
    JHIPSTER_APP_NAME: jhipster-${JHIPSTER_NAMESPACE}
    JHIPSTER_POSTGRES_NAME: postgres-${JHIPSTER_NAMESPACE}
    JHIPSTER_FINAL_URL: gr2.eqldog.space
    JHIPSTER_IMAGE_FULL_NAME: ${IMAGE_FULL_PATH_PROD}
    KUBECTL_APPLY_COMMAND: ${KUBECTL_APPLY_COMMAND_DEFAULT}
  environment:
    name: prod
    url: http://${JHIPSTER_FINAL_URL}
    on_stop: cleanup:delete-prod

### Cleanup ###
cleanup:delete-dev:
  extends: .deploy
  stage: cleanup
  needs:
    - deploy:dev
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
  variables:
    JHIPSTER_NAMESPACE: dev-${CI_COMMIT_REF_SLUG}
    JHIPSTER_APP_NAME: dev-jhipster-${CI_COMMIT_REF_SLUG}
    JHIPSTER_POSTGRES_NAME: disabled
    JHIPSTER_FINAL_URL: ${CI_COMMIT_REF_SLUG}.dev.gr2.eqldog.space
    JHIPSTER_IMAGE_FULL_NAME: ${IMAGE_FULL_PATH_DEV}
    SPRING_DATASOURCE_URL: jdbc:postgresql://localhost:5432/jhipsterSampleApplication
    SPRING_LIQUIBASE_URL: jdbc:postgresql://localhost:5432/jhipsterSampleApplication
    KUBECTL_APPLY_COMMAND: ${KUBECTL_DELETE_COMMAND_DEFAULT} --selector=app!=${JHIPSTER_POSTGRES_NAME}
  environment:
    name: review/dev-${CI_COMMIT_REF_SLUG}
    action: stop

cleanup:delete-ppd:
  extends: .deploy
  stage: cleanup
  needs:
    - deploy:ppd
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
  variables:
    JHIPSTER_NAMESPACE: ppd
    JHIPSTER_APP_NAME: jhipster-${JHIPSTER_NAMESPACE}
    JHIPSTER_POSTGRES_NAME: postgres-${JHIPSTER_NAMESPACE}
    JHIPSTER_FINAL_URL: ${JHIPSTER_NAMESPACE}.gr2.eqldog.space
    JHIPSTER_IMAGE_FULL_NAME: ${IMAGE_FULL_PATH_PROD}
    KUBECTL_APPLY_COMMAND: ${KUBECTL_DELETE_COMMAND_DEFAULT}
  environment:
    name: ppd
    action: stop

cleanup:delete-prod:
  extends: .deploy
  stage: cleanup
  needs:
    - deploy:prod
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: manual
  variables:
    JHIPSTER_NAMESPACE: prod
    JHIPSTER_APP_NAME: jhipster-${JHIPSTER_NAMESPACE}
    JHIPSTER_POSTGRES_NAME: postgres-${JHIPSTER_NAMESPACE}
    JHIPSTER_FINAL_URL: gr2.eqldog.space
    JHIPSTER_IMAGE_FULL_NAME: ${IMAGE_FULL_PATH_PROD}
    KUBECTL_APPLY_COMMAND: ${KUBECTL_DELETE_COMMAND_DEFAULT}
  environment:
    name: prod
    action: stop

### Templates ###
# Install kubectl and envsubst, create a yaml file based on template
.build:jar-script-prod-template:
  stage: build
  script:
    # - ./mvnw -Pprod clean package
    - ${MVNW_COMMAND}
    - echo $(basename $(ls -1 target/*.jar | head -n 1)) > jar_file_name.txt # Create a txt file with the exact name of the .jar
  artifacts:
    untracked: false
    when: on_success
    expire_in: 7 days
    paths:
      - target/*.jar
      - "jar_file_name.txt" # export the .txt file to be able to use the .jar exact file name later

.kaniko:template:
  stage: kaniko
  image:
    name: gcr.io/kaniko-project/executor:v1.14.0-debug
    entrypoint: [""]
    pull_policy: ["if-not-present", "always"]
  before_script: []
  script:
    - JAR_FILE_NAME=$(cat jar_file_name.txt)
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --build-arg JAR_FILE=${JAR_FILE_NAME}
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${JHIPSTER_IMAGE_FULL_NAME}"

.deploy:
  cache: []
  image:
    name: debian:stable-slim
    pull_policy: ["if-not-present", "always"]
  before_script:
    # Install Kubectl -- https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
    - apt-get update && apt-get install -y apt-transport-https ca-certificates curl
    - apt-get -y install gnupg
    - curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
    # This overwrites any existing configuration in /etc/apt/sources.list.d/kubernetes.list
    - echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | tee /etc/apt/sources.list.d/kubernetes.list
    - apt-get update && apt-get install -y kubectl
    # Install Envsubst -- https://manpages.debian.org/bullseye/gettext-base/envsubst.1.en.html
    - apt-get install -y gettext-base
    - kubectl config use-context P-why/jhipster-2:kubernetagent
  stage: deploy
  script:
    - envsubst < kubernetes/duo-jhipster-deployment-template.yaml > kubernetes/duo-jhipster-deployment.yaml
    - ${KUBECTL_APPLY_COMMAND}

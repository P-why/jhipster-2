FROM amazoncorretto:11.0.21-al2023-headless

ARG JAR_DIR=jhipsterapp
# ARG JAR_FILE=jhipster-sample-application-0.0.1-SNAPSHOT.jar
ARG JAR_FILE

RUN mkdir ${JAR_DIR}
COPY target/*.jar ${JAR_DIR}

# ENV POSTGRES_URL=localhost

# ENV SPRING_DATASOURCE_URL=jdbc:postgresql://${POSTGRES_URL}:5432/jhipsterSampleApplication
# ENV SPRING_LIQUIBASE_URL=jdbc:postgresql://${POSTGRES_URL}:5432/jhipsterSampleApplication

ENV JAR_PATH="${JAR_DIR}/${JAR_FILE}"

EXPOSE 8080

CMD java -jar ${JAR_PATH}